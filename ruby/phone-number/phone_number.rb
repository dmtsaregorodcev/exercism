class PhoneNumber
    WRONG_NUMBER = '0000000000'
    def initialize(number)
      @number = parse(number)
    end

    def parse(str)
        number = str.scan(/\d/)
        return WRONG_NUMBER if number.size < 10
        return WRONG_NUMBER if number.size > 11
        return WRONG_NUMBER if str.scan(/^[\d\s().-]+$/).empty?
        return WRONG_NUMBER if number.size > 10 && number[0] != '1'
        number.join
    end

    def number
        return @number[1, 10] if @number.size > 10
        @number
    end

    def area_code
        @number[0, 3]
    end 

    def to_s
        number = @number.size > 10 ? @number[1, 10] : @number
        "(#{number[0, 3]}) #{number[3, 3]}-#{number[6, 4]}"
    end          
end