class School
    def initialize
        @store = Hash.new{|h,k| h[k]=[]}
    end

    def students(grade)
        @store[grade].sort
    end

    def add(student,grade)
        @store[grade] << student
        self
    end

    def students_by_grade
        @store.sort.map{|key,arr| {grade: key, students: arr.sort}}
    end
end