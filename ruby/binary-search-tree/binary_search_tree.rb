class Bst
    attr_reader :data,:left,:right
    def initialize(data)
        @data = data
        @left = nil
        @right = nil
    end

    def right?
        @right.nil?
    end

    def left?
        @left.nil?
    end

    def insert(value)
        if value > @data
            right? ? @right = Bst.new(value) : @right.insert(value)
        else
            left? ? @left = Bst.new(value) : @left.insert(value)
        end  
    end

    def each(&block)
        enum = Enumerator.new do |yi|
            @left.each{|e| yi << e} if !left?
            yi << data
            @right.each{|e| yi << e} if !right?
        end
        enum.each(&block)
    end    
     
end