class Phrase
    def initialize(text)
        @text = text
    end

    def word_count
        @text.downcase.gsub(/['"](\w+)['"]/,' ').split(/[^\w']+/).each_with_object(Hash.new(0)){|word,h| h[word] += 1}
    end    
end    