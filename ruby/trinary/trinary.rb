class Trinary
    def initialize(number)
        @number = number
    end

    def to_decimal
        return 0 unless @number.scan(/[0-2]/).size == @number.size
        @number.reverse.chars.each_with_index.inject(0){|acc,(v,k)| acc += v.to_i*3**k}
    end    
end

module BookKeeping
  VERSION = 1 # Where the version number matches the one in the test.
end  