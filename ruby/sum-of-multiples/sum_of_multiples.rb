class SumOfMultiples
    def initialize(*args)
        @numbers = args
    end

    def to(limit)
        (1..limit-1).select{|number| @numbers.any?{|n| number % n == 0} }.reduce(0,&:+)
    end      
end    