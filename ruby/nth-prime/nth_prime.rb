class Prime
    def self.nth(number)
        raise ArgumentError if number <= 0
        int_max = (2**(0.size * 8 -2) -1)
        count_prime = 0
        2.upto(int_max) do |item|
            count_prime +=1 if is_prime?(item)
            return item if count_prime == number
        end
        raise ArgumentError
    end

    def self.is_prime?(item)
        2.upto(Math.sqrt(item).to_i) do |elemen|
            return false if item % elemen == 0
        end  
        true
    end    
end    