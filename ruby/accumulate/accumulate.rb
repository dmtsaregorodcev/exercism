class Array
    def accumulate(&block)
        result = []
        self.each{|value| result<<block.call(value)}
        result
    end    
end

