class PrimeFactors
    def self.for(number)
        result = []
        div = 2
        while number > 1 do
            if number % div == 0
                number /= div
                result << div
                div = 2
            else
                div += 1    
            end    
        end    
        result
    end    
end    