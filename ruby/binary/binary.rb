class Binary
    def self.to_decimal(text)
        raise ArgumentError if text.scan(/^[0-1]+$/).empty?
        text.chars.reverse.each_with_index.inject(0){|acc,(value,key)| acc += value.to_i * 2 ** key}
    end    
end    