class Grains
    def self.square(number)
        raise ArgumentError if number <= 0 || number > 64
        power = number - 1
        2 ** power
    end

    def self.total
       elements = []
       1.upto(64){|e| elements<< square(e)}
       elements.reduce(:+) 
    end
end    